from random import sample
from collections import deque, OrderedDict

class DominoCollection:
    def __init__(self, starting_dominoes=set()):
        tp = type(starting_dominoes)
        if tp == set:
            self.dominoes = starting_dominoes
        elif tp == tuple:
            self.dominoes = {starting_dominoes}
        elif tp == list:
            self.dominoes = set(starting_dominoes)
        else:
            raise TypeError("DominoCollection constructor argument(s) need to be tuples or sets")


    def has_this_domino(self, domino):
        return domino in self.dominoes

    def has_no_dominoes(self):
        return len(self.dominoes) == 0

    def count(self):
        return len(self.dominoes)

    def all_points(self):
        return sum(sum(domino) for domino in self.dominoes)

    def points_on(self, domino):
        return sum(domino)

    def clear(self):
        self.dominoes = set()


    def show(self, msg=""):
        if self.dominoes == None:
            print("No double has been played")
            return
        if len(self.dominoes) > 1:
            if not all(self.dominoes):
                print("Error NoneType in dominoes")
            else:
                print(f"{msg}: {sorted(self.dominoes)}")
        else:
            print(f"{msg}: {self.dominoes}")


    def take_from_randomly(self, quantity):
        if len(self.dominoes) == 0:
            print("No More Dominoes, can't take a random one")
            return
        dominoes_from = sample(self.dominoes, quantity)
        for a_domino in dominoes_from:
            self.dominoes.remove(a_domino)
        print(f"Taken domino(s):{dominoes_from}")
        if len(dominoes_from) == 1:
            return dominoes_from[0]
        else:
            return dominoes_from

    def remove_domino(self, domino):
        if not self.has_this_domino(domino):
            # raise IndexError("Domino not in set thus can't be removed. Might be a to do this")
            print(f"\n============================ {domino} not in set thus can't be removed. currently passing b4 raised error ============================ \n")
        else:
            self.dominoes.remove(domino)


    def receives(self, domino):
        if not domino:
            print("No domino recieved")
        else:
            # print(f"Id @ recieves: {hex(id(domino))}")
            self.dominoes.add(domino)


class Player(DominoCollection):

    def __init__(self, starting_dominoes, name="default"):
        super().__init__(starting_dominoes)
        self.name = name
        self.has_played = False
        self.last_domino_played = None

    def show(self):
        super().show(self.name)


    def plays(self, domino, receiving_collection, end=-1):
        '''
        left = 0, right = 1 and for first domino played => -1
        '''
        if type(receiving_collection).__name__ != "PlayingArea":
            raise TypeError("Domino must be played to a PlayingArea")
        self.remove_domino(domino)
        if end == 0:
            receiving_collection.add_left_end(domino)
        elif end == 1:
            receiving_collection.add_right_end(domino)
        elif end == -1:
            receiving_collection.add_first(domino)
        else:
            raise ValueError("An end must be specified when playing a domino")
        self.has_played = True
        self.last_domino_played = domino


class PlayingArea(DominoCollection):
    """docstring for Pool"""
    def __init__(self):
        self.dominoes = deque()
        self.ends = [None, None]
        self.game_log = []

    def show_ends(self):
        print(f"Left end: {self.ends[0]}")
        print(f"Right end: {self.ends[1]}")

    def turn_around(self, domino):
        return tuple(reversed(domino))


# I could have possibly made add_to_left() and right_end() one function and passed
# a flag to indicate which end, but then I'd have probably have to write a helper
# function and still test for append() or appendleft() for deque, so have left as is
    def add_left_end(self, domino):
        if domino[1] != self.ends[0]:
            domino = self.turn_around(domino)
            if domino[1] != self.ends[0]:
                raise ValueError(f"Domino {domino} can not be added to left end,  no match")
        self.dominoes.appendleft(domino)
        self.ends[0] = domino[0]


    def add_right_end(self, domino):
        if domino[0] != self.ends[1]:
            domino = self.turn_around(domino)
            if domino[0] != self.ends[1]:
                raise ValueError(f"Domino {domino} can not be added to RIGHT end,  no match")
        self.dominoes.append(domino)
        self.ends[1] = domino[1]


    def add_first(self, domino):
        if domino[0] != domino[1]:
            raise ValueError("First domino should be a double")
        self.ends[0] = domino[0]
        self.ends[1] = domino[1]
        self.add_right_end(domino)


    def show(self, msg):
        print("Dominoes played:", end=" ")
        for i, d in enumerate(self.dominoes):
            print(f"{d}", end="")
        print()