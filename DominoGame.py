from enum import IntEnum
from typing import List, Callable, Tuple
from dominoes import DominoCollection, Player, PlayingArea
import helperfunctions as hf
from strategyfunctions import (random_selection, highest_domino, highest_total_value,
                                domino_player_has_most_of)

class ReturnSignal(Exception):
    pass


class Game:

    # this enum is only used in Game, thus defined within Game
    class End(IntEnum):
        LEFT = 0
        RIGHT = 1

    # END = {'left' : 0, 'right' : 1, 0 : 'left', 1 :'right'}

    # Dictionary of strategy functions for type of domino game play and associated name string
    # for the player playing that strategy, eg. player.name 'Mosty' will be using
    # the domino_player_has_most_of() function from strategyfunctions.
    STRATEGIES = {'Mosty' : domino_player_has_most_of, 'Highness' : highest_domino,
                    'Totty' : highest_total_value, 'Randy' : random_selection}



    def __init__(self, max_double, num_of_players, starting_number_dominoes):
        if max_double < 2:
            raise ValueError("Please use a double 2 or more set for a game of dominoes")
        if num_of_players < 1:
            raise ValueError("A game with no players is a bit boring ! It's just a set of pieces.")

        # Can't check last error until pool has been created, too lazy to calc otherwise
        self.pool = DominoCollection(self.__create_set_of_dominoes(max_double))
        if (num_of_players * starting_number_dominoes) > self.pool.count():
            raise ValueError(f"Pool is smaller than the starting number of"
                                f" dominoes for all the players")
        self.players = []
        self.score_board = []
        self.dominoes_played = PlayingArea()
        self.max_double = max_double

        p_names = [k for k in Game.STRATEGIES.keys()] # p_names = player names

        # Players pick their starting number of dominoes
        for a_player_idx in range(0, num_of_players):
            some_dominoes = self.pool.take_from_randomly(starting_number_dominoes)
            self.players.append(Player(some_dominoes, p_names[a_player_idx]))


    def __create_set_of_dominoes(self, max_double):
        '''
        Parameters
        ----------
        max_double:         Highest double in the set of dominoes.

        Returns
        -------
        A set containing all the dominoes as tuples ((1, 2) or (6, 6)) from
        (max_double, max_double) down to (0, 0) ie (blank, blank) 0 <=> blank.
        '''
        dominoes = {(0, 0)}
        for top in range(0, max_double + 1):
            for bottom in range(0, top + 1):
                a_domino = (top, bottom)
                dominoes.add(a_domino)
        return dominoes


    def __pick_up_a_domino_until_top_double(self, player : Player):
        '''
        player picks up a domino until they pick up whatever double is specified by
        self.max_double. In a game of dominoes, if nobody has the top double, after
        picking their initial dominoes from the pool, each player picks one domino
        until somebody picks up the top double.

        Parameters
        ----------
        player:             player who's current turn it is.

        Returns
        -------
        None.
        '''
        top_double = (self.max_double, self.max_double)
        player.receives(self.pool.take_from_randomly(1))
        if player.has_this_domino(top_double):
            player.plays(top_double, self.dominoes_played)
        return


    def __create_play_options(self, player : Player) -> List[DominoCollection]:
        '''
        Returns a list of potential dominoes that can be played at either end. For example,
        the last dominoe played on the left end is (1, 2) ie dominoes to be played on the left
        have to contain a 1. Player has dominoes (1, 1) (1, 5) (2, 3) (4, 3) therefore this function
        would return end_possibilities[0] = {(1, 1), (1, 5)}. If the last domino played on
        the right end was (6, 3), 3 being the right end then this function would return
        end_possibilities[1] = {(2, 3), (4, 3)}

        Parameters
        ----------
        player:             player currently playing this turn.

        Returns
        -------
        end_possibilities:   A list of len 2 containing DominoCollections.
        [0] being the left end and [1] being the right end.
        '''
        End = self.End
        rtcv = hf.return_tuples_containing_value

        # See if player has any dominoes that match either end
        left = rtcv(player.dominoes, self.dominoes_played.ends[End.LEFT])
        right = rtcv(player.dominoes, self.dominoes_played.ends[End.RIGHT])

        # and if they do create a list of those dominoes, [0] being dominoes that can be
        # played on the left end & [1] dominoes for the right
        end_possibilities = [DominoCollection(left), DominoCollection(right)]

        # If there aren't any dominoes that match either end, pick up until there's a match
        # unless the pool's empty...
        while (end_possibilities[End.LEFT].has_no_dominoes() and
                end_possibilities[End.RIGHT].has_no_dominoes()):
            if self.pool.has_no_dominoes():
                raise ReturnSignal()

            # New domino from the pool
            player.receives(self.pool.take_from_randomly(1))

            # Keep track of how dominoes player has picked up for game log
            player.picked_up_this_turn += 1
            player.show()

            # recalculate left and right possibilities, because player's domnoes have changed
            left = rtcv(player.dominoes, self.dominoes_played.ends[End.LEFT])
            right = rtcv(player.dominoes, self.dominoes_played.ends[End.RIGHT])
            end_possibilities = [DominoCollection(left), DominoCollection(right)]

        end_possibilities[0].show("Left End possible dominoes to play: ")
        end_possibilities[1].show("Right End possible dominoes to play: ")
        return end_possibilities


    def __have_a_turn(self, player, function:Callable[[List[DominoCollection], PlayingArea, Player], Tuple]):
        '''
        Have a turn in a game of dominoes. If a double isn't down pick up 1 domino.
        If there is a playing area self.__create_play_options() gives the player options to play either
        extant or by picking up and then a domino is played using the strategy function if
        there's more than one domino that can be played. domino_to_play is then played, playing area
        and players own dominoes are updated.

        If the pool has no dominoes and the player needs to pick up, then the player misses their go.
        Can't pick from an empty pool. A ReturnSignal is raised an function returns early.

        Parameters
        ----------
        player:                 current player having their turn
        function:               strategy function imported from external module that returns a domino to
                                be played given possible dominoes to be played from each end (there could
                                be just one possible option) dominoes played (i.e. playing area) and
                                the dominoes the current player has (as player is a collection of
                                dominoes with a name !)

        Returns
        -------
        None
        '''
        End = self.End
        # ends=None means that the first domino (double max) hasn't been played yet
        if self.dominoes_played.ends[End.LEFT] == None:
            self.__pick_up_a_domino_until_top_double(player)
            return

        try:
            possibles_from_each_end = self.__create_play_options(player)
        except ReturnSignal:
            return

        domino_to_play = function(possibles_from_each_end, self.dominoes_played, player)

        # calculating new ends
        for an_end in (self.End):
            if possibles_from_each_end[an_end].has_this_domino(domino_to_play):
                player.plays(domino_to_play, self.dominoes_played, an_end)
                print(f"Playing this domino {domino_to_play}")
                return   # if ends are both the same need to exit


    def __who_goes_first(self) -> int:
        '''
        Return index of the next person after whoever plays the top double.
        Who ever has the top double in dominoes goes first, so the next person
        to go is required

        Returns
        -------
        index of the player whose turn it is next.
        '''
        top_double = (self.max_double, self.max_double)
        for idx, a_player in enumerate(self.players):
            if a_player.has_this_domino(top_double):
                a_player.show()
                print(f"---------- Double {self.max_double} played ! ----------")
                a_player.plays(top_double, self.dominoes_played)
                return idx + 1 if idx + 1 < len (self.players) else 0
        return 0


    def __game_won_or_drawn(self, player, players_played):
        '''
        Decide state of the game. If the game has been either won or the pool is empty
        and no-one can play a domino and the game is thus a draw, then self.__game_won_or_drawn()
        raises a ReturnSignal else nothing happens.

        Parameters
        ----------
        player:                 current player
        players_played:         a list containing whether each player has played a domino or not.
                                Needed for working out whether the game is in a drawn state.

        Returns
        -------
        None
        '''
        if player.has_no_dominoes():
            print(f"\n================================= {player.name} has WON =================================")
            self.__scores()
            raise ReturnSignal()
        if self.pool.has_no_dominoes():
            if not any(players_played):
                print(f"================================= Game Over, it's a Draw ! =================================")
                print('players_played should be [F, F]:',players_played)
                self.__scores()
                raise ReturnSignal()


    def __scores(self):
        '''
        Creates a score_board,  which is a list of tuples, each tuple being player's name,
        amount of dominoes and points on those dominoes.
        Also prints out the game log and score board.
        '''
        self.players.sort(key=lambda player: player.all_points())
        print("\n--------------------------- SCORES ---------------------------")
        for a_player in self.players:
            print(f"{a_player.name}: Number of dominoes: {a_player.count()} Total points: "
                    f"{a_player.all_points()}")
            self.score_board.append(tuple((a_player.name,a_player.count(),a_player.all_points())))
        for idx, a_turn in enumerate(self.dominoes_played.game_log):
            print(f"Turn: {(idx + 1)}. Player: {a_turn}")


    def play(self):
        '''
        Play a game of dominoes: Decide who goes first. If a player has the top double they go
        first, play passes to next player. If no-one has the top double, each player takes 1
        domino until the top double is picked up and then played. Play passes to the next player,
        if they can play a domino they play else they pick up domino(es) until they can.
        If the player has more than 1 domino to play, then using the Game.STRATEGIES dictionary,
        that contains the different strategy functions, a domino can be selected from the
        possible domnoes that a player has to play using the appropriate function.
        Scores, score board and game log are updated each turn.
        If the game is won or drawn ReturnSignal is raised.
        '''
        print("\n------------------Who goes first---------------------------------------\n")
        first_player = self.__who_goes_first()
        new_player_order = hf.wrap_around_list_at_index(self.players, first_player)
        for a_player in new_player_order:
                a_player.show()
        print("\n------------------------Now playing---------------------------------\n")
        players_played = [False for each_player in new_player_order]
        while True:
            for idx, a_player in enumerate(new_player_order):
                a_player.show()
                a_player.has_played = False

                # Choose a function from strategyfunctions.py
                a_player.picked_up_this_turn = 0
                self.__have_a_turn(a_player, Game.STRATEGIES[a_player.name])
                if not self.dominoes_played.has_no_dominoes():
                    self.dominoes_played.game_log.append({a_player.name:{
                                                        'picked_up' : a_player.picked_up_this_turn,
                                                        'played' : a_player.last_domino_played}})

                players_played[idx] = a_player.has_played
                print(f"(After the first double played) {a_player.name} has actually picked up or played a domino ?: {players_played[idx]}")
                self.dominoes_played.show("Dominoes played: Current Game:-")
                self.pool.show("Pool: ")
                try:
                    self.__game_won_or_drawn(a_player, players_played)
                except ReturnSignal:
                    return
                print()
                # input("Press enter to continue")
