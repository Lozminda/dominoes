import unittest
from collections import deque
from DominoGame import Game, ReturnSignal
from dominoes import DominoCollection, Player

class ResourcesClassForTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.NUMBER_OF_PLAYERS = 4
        cls.game4 = Game(4, cls.NUMBER_OF_PLAYERS, 1)
        cls.entire_set_double_4 = {(3, 2),(0, 0),(3, 3),(3, 0),(3, 1),(4, 4),(2, 1),(2, 0),
                                    (4, 3),(2, 2),(4, 2),(1, 0),(4, 1),(1, 1),(4, 0)}

class TestGameConstructor(ResourcesClassForTest):

    def setUp(self):
        self.game1 = Game(6, 2, 7)


    def test_instance_variable_types(self):
        self.assertIsInstance(self.game1.players[0].dominoes, set)
        self.assertIsInstance(self.game1.pool, DominoCollection)
        self.assertIsInstance(self.game1.dominoes_played, DominoCollection)


    def test_instance_variables_initial_condition(self,):
        self.assertEqual(len(self.game1.players[0].dominoes), 7)
        self.assertEqual(len(self.game1.players), 2)
        self.assertEqual(self.game1.dominoes_played.dominoes, deque())


class TestMethodCreateSetOfDominoes(ResourcesClassForTest):
    def test_return_result(self):
        game = Game(4, 2, 7)
        self.assertEqual(game._Game__create_set_of_dominoes(4), self.entire_set_double_4)


class TestMethodGameWonOrDrawn(ResourcesClassForTest):
    def test_for_win_custom_return_exception_should_throw(self):
        p1 = Player(set())
        p1.name = "Winner"
        game = Game(2, 1, 1)

        with self.assertRaises(ReturnSignal) as context:
            game._Game__game_won_or_drawn(p1, [False])
        self.assertTrue('' in str(context.exception))

    @unittest.expectedFailure
    def test_when_theres_no_winner_function_shouldnt_throw(self):
        p2 = Player((1, 2))
        p2.name = "Looser!"
        game = Game(2, 1, 1)
        with self.assertRaises(ReturnSignal) as context:
            game._Game__game_won_or_drawn(p2, [False])
        self.assertFalse('' in str(context.exception))

    def test_game_drawn_function_should_throw(self):
        self.game4.pool.dominoes.clear()
        with self.assertRaises(ReturnSignal) as context:
            self.game4._Game__game_won_or_drawn(self.game4.players[0], [False, False, False, False])
        self.assertTrue('' in str(context.exception))

    @unittest.expectedFailure
    def test_call_function_when_game_not_drawn_no_throw(self):
        with self.assertRaises(ReturnSignal) as context:
            self.game4._Game__game_won_or_drawn(self.game4.players[0], [False, False, False, False])
        self.assertTrue('' in str(context.exception))


class TestMethodWhoGoesFirst(ResourcesClassForTest):
    def test_return_boundries(self):
        self.assertLess(self.game4._Game__who_goes_first(), self.NUMBER_OF_PLAYERS + 1)
        self.assertGreaterEqual(self.game4._Game__who_goes_first(), 0)


    def test_player_with_top_double_goes_first(self):
        TEST_INDEX_1 = 0
        TEST_INDEX_2 = 2
        for p in self.game4.players:
            if p.has_this_domino((4, 4)):
                p.remove_domino((4, 4))
                print("Double 4 removed! This should only be once")
        self.game4.players[TEST_INDEX_1].receives((4, 4))
        self.assertEqual(self.game4._Game__who_goes_first(), TEST_INDEX_1 + 1)
        self.game4.players[TEST_INDEX_1].remove_domino((4, 4))
        self.game4.players[TEST_INDEX_2].receives((4, 4))
        self.assertEqual(self.game4._Game__who_goes_first(), TEST_INDEX_2 + 1)


class TestPrepareToPlay(ResourcesClassForTest):

    def test_return_result(self):
        for p in self.game4.players:
            for domino in self.entire_set_double_4:
                if p.has_this_domino(domino):
                    p.remove_domino(domino)     # No player should have any dominoes

        # game left end will be 4 and the right end 3
        self.game4.dominoes_played.add_first((4, 4))
        self.game4.dominoes_played.add_right_end((4, 3))

        # player[0] has these dominoes (1, 3), (2, 3), (2, 4) & (1, 1)
        self.game4.players[0].receives((1, 3))
        self.game4.players[0].receives((2, 3))
        self.game4.players[0].receives((2, 4))
        self.game4.players[0].receives((1, 1))

        [left_end, right_end] = self.game4._Game__create_play_options(self.game4.players[0])

        # so player[0] will be able to play (2, 4) on the left end and (2, 3) & (1, 3)
        # on the right and playing (1, 1) won't be an option
        self.assertIn((2, 4), left_end.dominoes)
        self.assertEqual(len(left_end.dominoes), 1)
        self.assertIn((2, 3), right_end.dominoes)
        self.assertIn((1, 3), right_end.dominoes)
        self.assertNotIn((1, 1), right_end.dominoes)
        self.assertNotIn((1, 1), left_end.dominoes)


class TestPickUpADominoUntilTopDouble(ResourcesClassForTest):
    def test_functionality(self):
        small_game = Game(2, 1, 1)
        small_game._Game__pick_up_a_domino_until_top_double(small_game.players[0])


class TestHaveATurn(unittest.TestCase):
    '''
    Not sure how to write a test for this function,
    it doesn't return anything,  but does raise an ReturnSignal
    from self.__create_play_options(player)
    '''
    pass


if __name__=='__main__':
    unittest.main()
