from random import randint, seed
from calendar import month_name

import unittest

# vvv This is just a note to myself! vvv
# python3 -m unittest -v helperfunctions


def return_other_value(tpl, value):
    '''
    return_other_value((1, 5), 1) => 5
    return_other_value((1, 5), 6) => raises an ValueError
    tpl parameter should be a pair
    '''
    if len(tpl) != 2:
        raise IndexError("Tuple passed to return_other_value must be a pair")
    if value not in tpl:
        raise ValueError(f"At return_other_value: {value} isn't in {tpl}")
    if tpl[0] == value:
        return tpl[1]
    return tpl[0]


# ---------------------- Test for return_other_value() ----------------------

class TestReturnOtherValue(unittest.TestCase):

    def test_functionality(self):
        rov = return_other_value
        self.assertEqual(rov((1, 4), 4), 1)

    def test_index_exception(self):
        rov = return_other_value
        with self.assertRaises(IndexError) as context:
            rov((1, ), 5)
        self.assertTrue('Tuple passed to return_other_value must be a pair' in str(context.exception))

    def test_index_exception(self):
        rov = return_other_value
        with self.assertRaises(ValueError) as context:
            rov((1, 2), 5)
        self.assertTrue("At return_other_value: 5 isn't in (1, 2)" in str(context.exception))

# ---------------------------------- End Test ----------------------------------


def wrap_around_list_at_index(l, index):
    '''
    Given l1 = [0, 1, 2, 3, 4, 5, 6, 7, 8] and index 3 returned list
    will be [3, 4, 5, 6, 7, 8, 0, 1, 2]
    wrap_around_list_at_index(l1, -1) => [8, 0, 1, 2, 3, 4, 5, 6, 7]
    if index (including -ve) is larger than than l then l is returned
    If l is not scriptable, Python will raise an exception.
    '''
    if index == 0:
        return l
    return [*l[index:],*l[:index]]


# ----------------------- Test for wrap_around_list_at_index() -----------------------

class TestWrapAroundListAtIndex(unittest.TestCase):

    def test_functionality(self):
        wrapa = wrap_around_list_at_index
        l1 = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        result = [3, 4, 5, 6, 7, 8, 0, 1, 2]
        self.assertEqual(wrapa(l1, 3), result)
        self.assertEqual(wrapa(l1, 0), l1)

    def test_Python_exception_for_non_collection_param(self):
        wrapa = wrap_around_list_at_index
        l1 = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        result = [3, 4, 5, 6, 7, 8, 0, 1, 2]
        with self.assertRaises(TypeError) as context:
            wrapa(4, 6)
        self.assertTrue("'int' object is not subscriptable" in str(context.exception))

# ---------------------------------- End Test ----------------------------------


def return_tuples_containing_value(collection, value):
    '''
    Where collection is a collection of tuples, returns a new list of all
    the tuples within the list that contain value. If there are no tuples
    with value in them returns empty list. Any exceptions are handled by
    Python
    '''
    tuple_list = []
    for tup in collection:
        if value in tup:
            tuple_list.append(tup)
    return tuple_list

# -------------------- Test for return_tuples_containing_value() --------------------

class TestReturnTuplesContainingValue(unittest.TestCase):

    def test_functionality(self):
        tcv = return_tuples_containing_value
        l1 = [(2, 0), (8, 4), (9, 2), (10, 2), (11, 9), (18, 16), (19, 8)]
        result = [(2, 0), (9, 2), (10, 2)]
        self.assertEqual(tcv(l1, 2), result)
        self.assertEqual(tcv(l1, 20), [])

    def test_Python_exception_for_non_collection_param(self):
        tcv = return_tuples_containing_value
        with self.assertRaises(TypeError) as context:
            tcv(4, 6)
        self.assertTrue("'int' object is not iterable" in str(context.exception))

# ---------------------------------- End Test ----------------------------------


def create_list_of_months(quantity, length):
    '''
    If the quantity of months required is long (say > 3), a number
    is appended to the month's name so that no two members of the list are the same

    Parameters
    ----------
    quantity: number of months (plus a number) in the returned list.
    length: the number of letters in each month name. There maybe extra
    numbers

    Returns
    -------
    A list of strings containing some or all of the letters of a month with some
    numbers appended to those name (possibly). In summary no 2 elements will
    be the same.
    '''
    seed()
    monthlist =  [month_name[randint(1, 12)][:length] for _ in range(0, quantity)]
    indices_of_copies = [i for i, x in enumerate(monthlist) if monthlist.count(x) > 1]
    if len(indices_of_copies) == 0:
        return monthlist
    for idx in indices_of_copies:
        monthlist[idx] = f"{monthlist[idx]}_{idx}"
    return monthlist


# ---------------------- Test for create_list_of_months() ----------------------

class TestCreateListOfMonths(unittest.TestCase):

    def test_functionality(self):
        ml = create_list_of_months
        self.assertEqual(len(ml(4, 4)), 4)
        test_list = ml(12, 5)
        self.assertEqual(len(test_list),len(set(test_list)))

# ---------------------------------- End Test ----------------------------------