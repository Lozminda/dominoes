import argparse
import contextlib
from DominoGame import Game

def play_game(double, num_of_players, starting_num_of_dominoes):
    '''
    Parameters
    ----------
    double:                     max domino double, for example 6 =>
                                a set of dominos that contain double 6
                                (6, 6) all the way down to (0,0). 9 =>
                                double 9 all the way down to (0,0)

    num_of_players:             how many players playing the game

    starting_num_of_dominoes:   How many dominoes each player starts
                                the game with


    Returns
    -------
    game.score_board            a list of tuples, each tuple being
                                the players name, number of dominoes remaining
                                and all the points on the remaining dominoes
                                added up
    '''
    game = Game(double, num_of_players, starting_num_of_dominoes)
    game.play()
    return game.score_board


def command_line_argument_parser():
    '''
    Returns argument specifications to the command line argument parser,
    using module argparse
    '''
    parser = argparse.ArgumentParser(
                    prog='PlayDominoes.py',
                    description='Plays 1 or more games of dominoes, automatically',
                    epilog='Written by Lozminda Lamarais')
    parser.add_argument("-v", "--verbose", help="Output the state of all dominoes",
                        action="store_true")
    parser.add_argument("-p", "--pause", help="Pause between games", action="store_true")
    parser.add_argument("-N", "--Number", dest="repeat", help="Number of times the game runs", type=int,
                        nargs="?", default=1)
    parser.add_argument("-P", "--People", dest="people", help="Number of people in the game, default is 4", type=int,
                        nargs="?", default=4)
    parser.add_argument("-d", "--double", dest="double", type=int, nargs="?", default=9,
                        help="The dominoe set, eg double 6, double 9, double 12. Default set to 9")
    parser.add_argument("-s", "--starting", dest="starting_dominoes", type=int, nargs="?", default=7,
                        help="Number of dominoes all the players start with, default = 7")
    return parser.parse_args()


def create_final_stats(grand_totals, count):
    '''
    Creates a score board dictionary composed of all of the results from each game by unpicking
    scores list and turning it into grandtotals dictionary

    Parameters
    ----------
    score:                      Is currentlyin global scope, which is naughty
    count:                      How many times the game has been played
    grand_totals:               dictionary containing the score board, with player
                                names acting as keys and values being their various
                                scores and average.

    Returns
    -------
    grand_totals:               A dictionary containing  whose keys are the players names
                                and values are their scores both number of dominoes they're
                                left with at the end of the game and the number of points on
                                those dominoes.
    '''
    if not grand_totals:
        for a_players_score in scores:
            # empty dictionary means first game, thus average will be players score¸(count=1)
            grand_totals[a_players_score[0]] = {
                'dominoes' : a_players_score[1], 'points' : a_players_score[2], 'average' : a_players_score[2]}
    else:
        for a_players_score in scores:
            grand_totals[a_players_score[0]]['dominoes'] += a_players_score[1]
            grand_totals[a_players_score[0]]['points'] += a_players_score[2]
            grand_totals[a_players_score[0]]['average'] = grand_totals[a_players_score[0]]['points'] / count
    return grand_totals


def output(grand_totals, count):
    game_str = "game"
    if count > 1:
        game_str = game_str + "s"
    print(f"-------------------- Scores after {count} {game_str} --------------------")
    for k, v in grand_totals.items():
        print(f"{k:<15} :  {v}")
    print()


if __name__ == '__main__':

    args = command_line_argument_parser()
    grand_totals = dict()
    for count in range(1, (args.repeat + 1)):
        if not args.verbose:
            # with contextlib.redirect_stdout(None):
            scores = play_game(args.double, args.people, args.starting_dominoes)
        else:
            scores = play_game(args.double, args.people, args.starting_dominoes)
            print(f"\n                                    Game number {count}, scores : {scores}")

        grand_totals = create_final_stats(grand_totals, count)
        if args.verbose and (count < args.repeat):  # so that grand_totals isn't output twice at end
            output(grand_totals, count)
        if args.pause:
            input("Press enter to continue")
    output(grand_totals, count)
