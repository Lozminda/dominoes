from typing import List, Tuple
from dominoes import Player, PlayingArea, DominoCollection
from helperfunctions import return_tuples_containing_value

# Domino functions must return a domino and take as it's parameters
# should be List[ DominoCollection, DominoCollection]
# except possibles from each end, the game playing area, players, ie your own dominoes.

def random_selection(possibles_from_each_end : List[DominoCollection], dominoes_played=None, player=None) -> Tuple:
    # possible_both_ends persists between players, even with
    # new construction everytime
    possible_both_ends = DominoCollection( possibles_from_each_end[0].dominoes |
                                            possibles_from_each_end[1].dominoes)

    # having got some possible dominoes to play, selecting one at random by taking
    # both ends and making them one group: possible_both_ends.
    possible_both_ends.show("Possible dominos to play: ")
    return possible_both_ends.take_from_randomly(1)


def highest_total_value(possibles_from_each_end : List[DominoCollection], dominoes_played=None, player=None) -> Tuple:
    '''
    Return domino with highest total score
    '''
    possible_both_ends = possibles_from_each_end[0].dominoes | possibles_from_each_end[1].dominoes
    return max(possible_both_ends, key=lambda tpl:sum(tpl))


def highest_domino(possibles_from_each_end : List[DominoCollection], dominoes_played=None, player=None) -> Tuple:
    '''
    Return the domino with of the highest number
    '''
    possible_both_ends = possibles_from_each_end[0].dominoes | possibles_from_each_end[1].dominoes
    return max(possible_both_ends)


def domino_player_has_most_of(possibles_from_each_end : List[DominoCollection], dominoes_played=None, player=None) -> Tuple:
    '''
    for example of the player has five dominoes with value 2,  player would play a domino
    so that 2 is on an end if possible. The logic being, if the player has a lot of twos,
    there are less twos for other players,  and therefore less oportunities for them to play

    parameters
    ----------


    Returns
    -------

    '''
    results = dict()
    possible_both_ends = possibles_from_each_end[0].dominoes | possibles_from_each_end[1].dominoes
    if len(possible_both_ends) == 1:        # No choice
        return possible_both_ends
    else:
        # return next(iter(possible_both_ends))
        for idx, end in enumerate(dominoes_played.ends):
            for a_domino in possibles_from_each_end[idx].dominoes:
                if a_domino[0] == end:
                    search = a_domino[1]
                else:
                    search = a_domino[0]
                # now look for the number of dominoes with the search in players
                # dominoes in results dictionary with domino as key
                results[a_domino] = len(return_tuples_containing_value(player.dominoes, search))
        return max(zip(results.values(), results.keys()))[1]